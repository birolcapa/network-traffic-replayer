﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using log4net;

namespace NetworkTrafficReplayer.ColasoftPacketPlayer
{
    /// <summary>
    /// Repat speed of the pcap file.
    /// </summary>
    public enum Speed
    {
        OneOverEightX = 1,
        OneOverFourX = 2,
        OneOverTwoX = 3,
        OneX = 4,
        TwoX = 5,
        FourX = 6,
        Burst = 7
    }

    /// <summary>
    /// Cola soft packet player wrapper
    /// </summary>
    internal class ColaSoftPacketPlayerWrapper
    {
        private const int s_BnClicked = 245;
        private const int s_CbSetcursel = 0x014E;
        private const int s_WmSettext = 12;
        private const int s_TbmSetpos = 0x0405;

        /// <summary>
        /// Logger for Colasoft Packet Player Wrapper
        /// </summary>
        private static readonly ILog s_Log = LogManager.GetLogger(typeof(ColaSoftPacketPlayerWrapper));

        /// <summary>
        /// Delegate for Running Colasoft Packet Plater
        /// </summary>
        /// <param name="window">Window Pointer</param>
        /// <param name="index">Colasoft NIC Combobox Index</param>
        /// <param name="pcap">Path of pcap file </param>
        /// <param name="speed">Speed of replaying pcap file</param>
        private delegate void RunColasoftPacketPlayerDelegate(IntPtr window, int index, string pcap, Speed speed);

        /// <summary>
        /// Run Colasoft Packet Player Asynchronously
        /// </summary>
        /// <param name="window">Window Ptr</param>
        /// <param name="index">Colasoft NIC Combobox Index</param>
        /// <param name="pcap">Path of pcap file</param>
        /// <param name="speed">Repat speed of the pcap file</param>
        /// <returns>Returns the status of an asynchronous operation</returns>
        public IAsyncResult RunColasoftPlayerAsync(IntPtr window, int index, string pcap, Speed speed)
        {
            RunColasoftPacketPlayerDelegate packetPlayerDelegate = PlayerPlay;

            // Create the callback delegate
            AsyncCallback packetPlayerCallback = PacketPlayerCallback;

            // Initiate the Asynchronous call passing in the callback delegate
            // and the delegate object used to initiate the call.
            IAsyncResult asyncResult = packetPlayerDelegate.BeginInvoke(window, index, pcap, speed,
                packetPlayerCallback, packetPlayerDelegate);
            return asyncResult;
        }

        /// <summary>
        /// Start Colasoft Packet Player Application
        /// </summary>
        public bool StartApp()
        {
            const string instalDir = @"C:\Program Files (x86)\Colasoft Packet Player 2.0\pktplayer.exe";
            if (!File.Exists(instalDir))
            {
                s_Log.Error("Colasoft Packet Player is not installed or installation directory is different than " +
                            instalDir);
                return false;
            }
            Process externalProcess = new Process
                                      {
                                          StartInfo =
                                          {
                                              FileName = instalDir,
                                              WindowStyle = ProcessWindowStyle.Normal
                                          }
                                      };
            return externalProcess.Start();
        }

        /// <summary>
        /// Get windows of Colasoft Packet Player Window
        /// </summary>
        /// <returns></returns>
        public List<IntPtr> GetColaSoftWindows()
        {
            List<IntPtr> handles = new List<IntPtr>();
            //var windows = new Dictionary<IntPtr, string>();
            foreach (KeyValuePair<IntPtr, string> window in GetOpenWindows())
            {
                IntPtr handle = window.Key;
                string title = window.Value;

                if (window.Value == "Colasoft Packet Player")
                {
                    //Get a handle for the Calculator Application main window
                    int hwnd = FindWindow(null, "Colasoft Packet Player");
                    if (hwnd == 0)
                    {
                        s_Log.Info("Not found!");
                    }
                    else
                    {
                        //windows[handle] = title;
                        handles.Add(handle);
                    }
                    s_Log.Info(handle + ": " + title + "\n");
                }
            }
            return handles;
        }

        /// <summary>
        /// Close Colasoft Packet Player Window
        /// </summary>
        /// <param name="handle"></param>
        public void CloseWindow(IntPtr handle)
        {
            IntPtr closeButton = GetCloseButton((int)handle);
            ClickCloseButton(closeButton);
        }
        
        /// <summary>Returns a dictionary that contains the handle and title of all the open windows.</summary>
        /// <returns>A dictionary that contains the handle and title of all the open windows.</returns>
        private static IDictionary<IntPtr, string> GetOpenWindows()
        {
            IntPtr shellWindow = GetShellWindow();
            Dictionary<IntPtr, string> windows = new Dictionary<IntPtr, string>();

            EnumWindows(delegate(IntPtr hWnd, int lParam)
            {
                if (hWnd == shellWindow) return true;
                if (!IsWindowVisible(hWnd)) return true;

                int length = GetWindowTextLength(hWnd);
                if (length == 0) return true;

                StringBuilder builder = new StringBuilder(length);
                GetWindowText(hWnd, builder, length + 1);

                windows[hWnd] = builder.ToString();
                return true;

            }, 0);

            return windows;
        }

        /// <summary>
        /// Packet Player Callback
        /// </summary>
        /// <param name="ar">AsyncState parameter</param>
        private void PacketPlayerCallback(IAsyncResult ar)
        {
            // Because you passed your original delegate in the asyncState parameter
            // of the Begin call, you can get it back here to complete the call.
            RunColasoftPacketPlayerDelegate packetPlayerDelegate = (RunColasoftPacketPlayerDelegate)ar.AsyncState;

            // Complete the call
            packetPlayerDelegate.EndInvoke(ar);
        }

        /// <summary>
        /// Play player
        /// </summary>
        /// <param name="window">Window Ptr</param>
        /// <param name="index">Colasoft NIC Combobox Index</param>
        /// <param name="pcap">Path of pcap file</param>
        /// <param name="speed">Repat speed of the pcap file</param>
        private void PlayerPlay(IntPtr window, int index, string pcap, Speed speed)
        {
            IntPtr handle = window;

            // Get combobox itself and set its value
            IntPtr comboBox = GetComboBox((int)handle);
            SetComboItem(index, comboBox);

            // Get textbox and set its value
            IntPtr textBox = GetTextBox((int)handle);
            SetText(pcap, textBox);

            // Get trackbar and set it to burst mode
            IntPtr burstButton = GetTrackBar((int)handle);
            SetTrackBarValue(burstButton, speed);

            // Wait for ui to handle events
            Thread.Sleep(1000);
            
            IntPtr statusTextBox = GetStatusTextBox((int)handle);

            // Get play button and Play Pcap
            IntPtr playButton = GetPlayButton((int)handle);
            ClickPlayButton(playButton);

            // Wait for ui to handle events
            Thread.Sleep(1000);

            string statusText = GetTextBoxText(statusTextBox);

            while (statusText != "Packet file playback stopped.")
            {
                statusText = GetTextBoxText(statusTextBox);
                Thread.Sleep(2000);
            }

            // Wait for ui to handle events
            Thread.Sleep(1000);

            // Get close button and close player
            IntPtr closeButton = GetCloseButton((int)handle);
            ClickCloseButton(closeButton);

            Thread.Sleep(1000);
        }
        
        /// <summary>
        /// Get play button of Colasoft
        /// </summary>
        /// <param name="hwnd">Window handle</param>
        /// <returns>Returns play button handle</returns>
        private IntPtr GetPlayButton(int hwnd)
        {
            //Get a handle for the "Play" button
            return FindWindowEx((IntPtr)hwnd, IntPtr.Zero, "Button", "Play");
        }

        /// <summary>
        /// Click play button
        /// </summary>
        /// <param name="hwndChild">Play button handle</param>
        private void ClickPlayButton(IntPtr hwndChild)
        {
            //send BN_CLICKED message
            SendMessage((int)hwndChild, s_BnClicked, 0, IntPtr.Zero);
        }

        /// <summary>
        /// Get Close Button
        /// </summary>
        /// <param name="hwnd">Window handle</param>
        /// <returns>Returns close button handle</returns>
        private IntPtr GetCloseButton(int hwnd)
        {
            //Get a handle for the "Close" button
            return FindWindowEx((IntPtr)hwnd, IntPtr.Zero, "Button", "Close");
        }

        /// <summary>
        /// Click Close Button
        /// </summary>
        /// <param name="hwndChild">Window handle</param>
        private void ClickCloseButton(IntPtr hwndChild)
        {
            //send BN_CLICKED message
            SendMessage((int)hwndChild, s_BnClicked, 0, IntPtr.Zero);
        }

        /// <summary>
        /// Get Combobox 
        /// </summary>
        /// <param name="hwnd">Window handle</param>
        /// <returns>Returns Combobox handle</returns>
        private IntPtr GetComboBox(int hwnd)
        {
            //Get a handle for the "Combobox"
            return FindWindowEx((IntPtr)hwnd, IntPtr.Zero, "ComboBox", null);
        }

        /// <summary>
        /// Set Combobox Index
        /// </summary>
        /// <param name="index">Combobox Index</param>
        /// <param name="handle">Combobox handle</param>
        private void SetComboItem(int index, IntPtr handle)
        {
            SendMessage(handle, s_CbSetcursel, index, "0");
        }

        /// <summary>
        /// Get TextBox
        /// </summary>
        /// <param name="hwnd">Window handle</param>
        /// <returns>Returns TextBox Handle</returns>
        private IntPtr GetTextBox(int hwnd)
        {
            return FindWindowEx((IntPtr)hwnd, IntPtr.Zero, "Edit", null);
        }

        /// <summary>
        /// Set text at textbox
        /// </summary>
        /// <param name="text">Text</param>
        /// <param name="handle">TextBox handle</param>
        private void SetText(string text, IntPtr handle)
        {
            string path = text;
            SendMessage(handle, s_WmSettext, IntPtr.Zero, path);
        }

        /// <summary>
        /// Get track bar
        /// </summary>
        /// <param name="hwnd">Window handle</param>
        /// <returns>Returns TrackBar handle</returns>
        private IntPtr GetTrackBar(int hwnd)
        {
            return FindWindowEx((IntPtr)hwnd, IntPtr.Zero, "msctls_trackbar32", "");
        }

        //private IntPtr GetTrackBarMaxVal(int hwnd)
        //{
        //    return SendMessage((IntPtr)hwnd, s_TbmGetrangemax, IntPtr.Zero, IntPtr.Zero);
        //}

        //private IntPtr GetTrackBarMinVal(int hwnd)
        //{
        //    return SendMessage((IntPtr)hwnd, s_TbmGetrangemin, IntPtr.Zero, IntPtr.Zero);
        //}

        /// <summary>
        /// Set TrackBar value
        /// </summary>
        /// <param name="handle">TrackBar handle</param>
        /// <param name="speed">Speed value</param>
        private void SetTrackBarValue(IntPtr handle, Speed speed)
        {
            int mode = (int)speed;
            SendMessage(handle, s_TbmSetpos, (IntPtr)1, (IntPtr)mode);
        }

        //private IntPtr GetProgressBar(int hwnd)
        //{
        //    return FindWindowEx((IntPtr)hwnd, IntPtr.Zero, "msctls_progress32", "");
        //}

        //private uint GetProgressBarValue(IntPtr handle)
        //{
        //    return (uint)SendMessage(handle, s_PbmGetpos, IntPtr.Zero, IntPtr.Zero);
        //}

        //private IntPtr GetPacketsSentTextBox(int hwnd)
        //{
        //    return FindWindowEx((IntPtr)hwnd, IntPtr.Zero, "Static", "0");
        //}

        /// <summary>
        /// Get TextBox's Status Label
        /// </summary>
        /// <param name="hwnd">Window Handle</param>
        /// <returns>Returns Status Label handle</returns>
        private IntPtr GetStatusTextBox(int hwnd)
        {
            return FindWindowEx((IntPtr)hwnd, IntPtr.Zero, "Static",
                "Please select an adapter and a packet file, Click Play button to start.");
        }

        /// <summary>
        /// Get text length of TextBox
        /// </summary>
        /// <param name="hTextBox">TextBox handle</param>
        /// <returns>Returns length of TextBox</returns>
        private static int GetTextBoxTextLength(IntPtr hTextBox)
        {
            // helper for GetTextBoxText
            const uint wmGettextlength = 0x000E;
            int result = SendMessage4(hTextBox, wmGettextlength,
              0, 0);
            return result;
        }

        /// <summary>
        /// Get text of TextBox
        /// </summary>
        /// <param name="hTextBox">TextBox Handle</param>
        /// <returns>Returns text of TextBox</returns>
        private static string GetTextBoxText(IntPtr hTextBox)
        {
            const uint wmGettext = 0x000D;
            int len = GetTextBoxTextLength(hTextBox);
            if (len <= 0) return null;  // no text
            StringBuilder sb = new StringBuilder(len + 1);
            SendMessage3(hTextBox, wmGettext, len + 1, sb);
            return sb.ToString();
        }

        // For public static IDictionary<IntPtr, string> GetOpenWindows()
        private delegate bool EnumWindowsProc(IntPtr hWnd, int lParam);

        [DllImport("user32.DLL")]
        private static extern bool EnumWindows(EnumWindowsProc enumFunc, int lParam);

        [DllImport("user32.DLL")]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32.DLL")]
        private static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.DLL")]
        private static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("user32.DLL")]
        private static extern IntPtr GetShellWindow();

        // For public Dictionary<IntPtr, string> GetColaSoftWindows()
        [DllImport("user32.DLL")]
        private static extern int FindWindow(String lpClassName, String lpWindowName);

        //For public HWND GetPlayButton(int hwnd)
        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle);

        // For public void ClickPlayButton(IntPtr hwndChild)
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SendMessage(int hWnd, int msg, int wParam, IntPtr lParam);

        // For public void SetComboItem(int index, IntPtr handle)
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        private static extern IntPtr SendMessage(IntPtr hWnd, uint msg, int wParam, string lParam);

        // For public int SetText(string text, IntPtr handle)
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        private static extern int SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, string lParam);

        // For public void SetTrackBarValue(IntPtr handle)
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

        // For private static int GetTextBoxTextLength(IntPtr hTextBox)
        [DllImport("user32.dll", EntryPoint = "SendMessage", CharSet = CharSet.Auto)]
        static extern int SendMessage3(IntPtr hwndControl, uint msg, int wParam, StringBuilder strBuffer);

        // For private static string GetTextBoxText(IntPtr hTextBox)
        [DllImport("user32.dll", EntryPoint = "SendMessage", CharSet = CharSet.Auto)]
        static extern int SendMessage4(IntPtr hwndControl, uint msg, int wParam, int lParam);
    }
}
