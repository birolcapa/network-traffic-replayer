﻿using System;
using System.Collections.Generic;
using System.Threading;
using log4net;
using NetworkTrafficReplayer.ColasoftPacketPlayer;

namespace NetworkTrafficReplayer
{
    /*! \mainpage Network Traffic Replayer
     * Network Traffic Replayer is a C# dll which utilizes Colasoft Packet Player UI.
     * 
     * Network Traffic Replayer is able to do following steps: 
     * - Open Colasoft Packet Player
     * - Select Network Interface Controller 
     * - Set Pcap File Path 
     * - Set Pcap Replay Speed
     * With these properties, there is no need to open Colasoft UI and do these steps by hand.
     * 
     * How to understand Network Interface Controller Combobox Index:
     * \image html NICIndex.png
     * Examples:
     * \snippet PacketPlayerTest.cs SampleUsageOfColasoft
    */

    /// <summary>
    /// PacketPlayer class.
    /// </summary>
    public class PacketPlayer
    {
        /// <summary>
        /// Logger for PacketPlayer class
        /// </summary>
        private static readonly ILog s_Log = LogManager.GetLogger(typeof(PacketPlayer));

        public PacketPlayer()
        {
            // Start colasoft player
            ColaSoftPacketPlayerWrapper colaSoft = new ColaSoftPacketPlayerWrapper();

            // Find all colasoft windows
            List<IntPtr> windows = colaSoft.GetColaSoftWindows();

            // Close them al.
            // So, end user will be able to sure that he have started all running instances
            foreach (IntPtr window in windows)
            {
                colaSoft.CloseWindow(window);
            }
        }
        
        /// <summary>
        /// Runs Colasoft Packet Player
        /// </summary>
        /// <param name="pcapFilePath">Path of pcap file</param>
        /// <param name="colasoftNicIndex">Colasoft Network Interface Controller Combobox Index</param>
        /// <param name="speed">Repat speed of the pcap file</param>
        /// <returns>Returns the status of an asynchronous operation</returns>
        public IAsyncResult RunColaSoftPlayer(string pcapFilePath, int colasoftNicIndex, Speed speed)
        {
            s_Log.Info("RunColaSoftPlayer is called");

            IAsyncResult pXAsyncResult = null;

            // pcap file should not be null
            if (pcapFilePath == null)
            {
                s_Log.Error("Pcap file cannot be null!");
                return null;
            }
            try
            {
                //define Colasoft Number
                const int portNumber = 1;

                // Start colasoft player
                ColaSoftPacketPlayerWrapper colaSoft = new ColaSoftPacketPlayerWrapper();

                //// Find all colasoft windows
                //List<IntPtr> windows = colaSoft.GetColaSoftWindows();

                //// Close them all
                //foreach (IntPtr window in windows)
                //{
                //    colaSoft.CloseWindow(window);
                //}

                // Start new colasoft
                for (int j = 0; j < portNumber; j++)
                {
                    if (colaSoft.StartApp())
                    {
                        continue;
                    }
                    s_Log.Error("Colasoft cannot be started.");
                    return null;
                }

                // Get windows 
                // Thread sleep is important for handles
                // handles should be ready after applications are started
                Thread.Sleep(3000);
                List<IntPtr> newWindows = colaSoft.GetColaSoftWindows();

                while (newWindows.Count < portNumber)
                {
                    newWindows = colaSoft.GetColaSoftWindows();
                    Thread.Sleep(1000);
                }

                // Start Port1 player
                pXAsyncResult = colaSoft.RunColasoftPlayerAsync(newWindows[0],
                    colasoftNicIndex, pcapFilePath, speed);
                return pXAsyncResult;

            }
            catch (Exception ex)
            {
                s_Log.Error("An exception is occured at RunColaSoftPlayer: \n" + ex);
                return pXAsyncResult;
            }
        }
    }
}
