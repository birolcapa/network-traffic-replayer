﻿using System;
using System.Threading;
using NetworkTrafficReplayer;
using NetworkTrafficReplayer.ColasoftPacketPlayer;
using NUnit.Framework;

namespace UnitTests
{
    /// <summary>
    /// PacketPlayerTest Class
    /// </summary>
    class PacketPlayerTest
    {
        [Test]
        public void RunColaSoftPlayerTestWithNullString()
        {
            PacketPlayer packetPlayer = new PacketPlayer();
            IAsyncResult result = packetPlayer.RunColaSoftPlayer(null, 0, Speed.FourX);
            Assert.Null(result);
        }

        /// <summary>
        /// Test for Speed OneOverEightX
        /// </summary>
        [Test]
        public void RunColaSoftPlayerTestSpeedOneOverEightX()
        {
            //! [SampleUsageOfColasoft]
            // Set pcap file path
            string pcapFile = @"..\..\sampleinput\arp-storm.pcap";
            
            // Set Combobox Index Value of Network Interface Controller of Colasoft UI
            int colasoftNNicComboboxIndex = 0;
            
            // Set speed of replaying pcap file
            Speed pcapReplaySpeed = Speed.OneOverEightX;
            
            // Create new Colasoft Instance
            PacketPlayer packetPlayer = new PacketPlayer();
            
            // Run colasoft packet player
            IAsyncResult result = packetPlayer.RunColaSoftPlayer(pcapFile, colasoftNNicComboboxIndex, pcapReplaySpeed);

            // Wait until replaying is done
            while (result.IsCompleted != true)
            {
                Thread.Sleep(1000);
            }
            //! [SampleUsageOfColasoft]
            Assert.IsNotNull(result);
        }

        [Test]
        public void RunColaSoftPlayerTestSpeedOneOverFourX()
        {
            string pcapFile = @"..\..\sampleinput\arp-storm.pcap";
            PacketPlayer packetPlayer = new PacketPlayer();
            IAsyncResult result = packetPlayer.RunColaSoftPlayer(pcapFile, 0, Speed.OneOverFourX);
            while (result.IsCompleted != true)
            {
                Thread.Sleep(1000);
            }
            Assert.IsNotNull(result);
        }

        [Test]
        public void RunColaSoftPlayerTestSpeedOneOverTwoX()
        {
            string pcapFile = @"..\..\sampleinput\arp-storm.pcap";
            PacketPlayer packetPlayer = new PacketPlayer();
            IAsyncResult result = packetPlayer.RunColaSoftPlayer(pcapFile, 0, Speed.OneOverTwoX);
            while (result.IsCompleted != true)
            {
                Thread.Sleep(1000);
            }
            Assert.IsNotNull(result);
        }

        [Test]
        public void RunColaSoftPlayerTestSpeedOneX()
        {
            string pcapFile = @"..\..\sampleinput\arp-storm.pcap";
            PacketPlayer packetPlayer = new PacketPlayer();
            IAsyncResult result = packetPlayer.RunColaSoftPlayer(pcapFile, 0, Speed.OneX);
            while (result.IsCompleted != true)
            {
                Thread.Sleep(1000);
            }
            Assert.IsNotNull(result);
        }

        [Test]
        public void RunColaSoftPlayerTestSpeedTwoX()
        {
            string pcapFile = @"..\..\sampleinput\arp-storm.pcap";
            PacketPlayer packetPlayer = new PacketPlayer();
            IAsyncResult result = packetPlayer.RunColaSoftPlayer(pcapFile, 0, Speed.TwoX);
            while (result.IsCompleted != true)
            {
                Thread.Sleep(1000);
            }
            Assert.IsNotNull(result);
        }

        [Test]
        public void RunColaSoftPlayerTestSpeedFourX()
        {
            string pcapFile = @"..\..\sampleinput\arp-storm.pcap";
            PacketPlayer packetPlayer = new PacketPlayer();
            IAsyncResult result = packetPlayer.RunColaSoftPlayer(pcapFile, 0, Speed.FourX);
            while (result.IsCompleted != true)
            {
                Thread.Sleep(1000);
            }
            Assert.IsNotNull(result);
        }

        [Test]
        public void RunColaSoftPlayerTestSpeedBurst()
        {
            string pcapFile = @"..\..\sampleinput\arp-storm.pcap";
            PacketPlayer packetPlayer = new PacketPlayer();
            IAsyncResult result = packetPlayer.RunColaSoftPlayer(pcapFile, 0, Speed.Burst);
            while (result.IsCompleted != true)
            {
                Thread.Sleep(1000);
            }
            Assert.IsNotNull(result);
        }

        [Test]
        public void RunColaSoftPlayerWithMultiplePcapFiles()
        {
            string pcapFile = @"..\..\sampleinput\arp-storm.pcap";
            string multiplePcapFiles = pcapFile + "\r\n" + pcapFile;
            PacketPlayer packetPlayer = new PacketPlayer();
            IAsyncResult result = packetPlayer.RunColaSoftPlayer(multiplePcapFiles, 0, Speed.Burst);
            while (result.IsCompleted != true)
            {
                Thread.Sleep(1000);
            }
            Assert.IsNotNull(result);
        }

        [Test]
        public void RunMultipleColaSoftPlayerWithMultiplePcapFiles()
        {
            string pcapFile = @"..\..\sampleinput\arp-storm.pcap";
            string multiplePcapFiles = pcapFile + "\r\n" + pcapFile;
            PacketPlayer packetPlayer = new PacketPlayer();
            IAsyncResult result = packetPlayer.RunColaSoftPlayer(multiplePcapFiles, 0, Speed.Burst);
            IAsyncResult result2 = packetPlayer.RunColaSoftPlayer(multiplePcapFiles, 1, Speed.Burst);
            while (result.IsCompleted != true || result2.IsCompleted != true)
            {
                Thread.Sleep(1000);
            }
            Assert.IsNotNull(result);
        }


    }
}
