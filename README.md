Network Traffic Replayer is a C# dll which utilizes Colasoft Packet Player UI.  

Network Traffic Replayer is able to do following steps:  

- Open Colasoft Packet Player  
- Select Network Interface Controller  
- Set Pcap File Path  
- Set Pcap Replay Speed  

With these properties, there is no need to open Colasoft UI and do these steps by hand.